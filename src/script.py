import pandas as pd
from datetime import datetime
from sklearn.metrics import mean_squared_error
import numpy as np
from sklearn.neighbors import KNeighborsRegressor

print("Reading file")
data = pd.read_csv('../data/parkings_measurements_202205040833.csv', delimiter=';', low_memory=False)

print("File read")

data['datetime'] = data.apply(lambda row: datetime.fromtimestamp(row.date_modified / 1000), axis=1)
data['day_of_week'] = data['datetime'].dt.day_of_week
data['year'] = pd.DatetimeIndex(data['datetime']).year
data['month'] = pd.DatetimeIndex(data['datetime']).month
data['day'] = pd.DatetimeIndex(data['datetime']).day
data['hour'] = pd.DatetimeIndex(data['datetime']).hour
data['minute'] = pd.DatetimeIndex(data['datetime']).minute

parking_one = data.loc[data['source_id'] == '534016']

parking_one_train = parking_one.loc[(parking_one['year'] < 2017) & (parking_one['year'] > 2014)]


def preprocess(df):
    df = df.drop(columns=['date_modified', 'datetime', 'available_spot_number', 'source', 'source_id'])
    df.fillna(0)
    return df


validate = parking_one.loc[(parking_one['year'] == 2018) & (parking_one['month'] == 3) & (parking_one['day'] < 16)]
validate = preprocess(validate)
X_validate = validate.drop(columns=['occupied_spot_number'])
Y_validate = validate['occupied_spot_number']

weights = {
    "closed_spot_number": 0.1,
    "total_spot_number": 0.1,
    "day_of_week": 100,
    "year": 0.5,
    "month": 10,
    "hour": 100000,
    "minute": 1,
    "day": 1
}
default_weights = {
    "closed_spot_number": 0.1,
    "total_spot_number": 0.1,
    "day_of_week": 100,
    "year": 0.5,
    "month": 10,
    "hour": 100000,
    "minute": 1,
    "day": 1
}


def distance(a, b):
    closed_spot_number = abs(a[0] - b[0])
    total_spot_number = abs(a[1] - b[1])
    day_of_week = min((a[2] - b[2]) % 7, (b[2] - a[2]) % 7)
    year = abs(a[3] - b[3])
    month = min((a[4] - b[4]) % 12, (b[4] - a[4]) % 12)
    day = min((a[5] - b[5]) % 31, (b[5] - a[5]) % 31)
    hour = min((a[6] - b[6]) % 24, (b[6] - a[6]) % 24)
    minute = min((a[7] - b[7]) % 60, (b[7] - a[7]) % 60)

    return closed_spot_number * weights['closed_spot_number'] + total_spot_number * weights['total_spot_number'] + \
           day_of_week * weights['day_of_week'] + year * weights['year'] + month * weights['month'] + hour * weights[
               'hour'] + \
           minute * weights['minute'] + day * weights['day']

parking_one_train = preprocess(parking_one_train)
Y_train = parking_one_train['occupied_spot_number']
X_train = parking_one_train.drop(columns=['occupied_spot_number'])

print("Measuring started")
stats = []
for crit in ["closed_spot_number",
             "total_spot_number",
             "day_of_week",
             "year",
             "month",
             "hour",
             "minute",
             "day"]:
    print(crit)
    for val in [0.1, 0.5, 1, 100, 10000]:
        print(val)
        weights[crit] = val
        nbrs = KNeighborsRegressor(n_neighbors=5, metric=distance)
        nbrs.fit(X_train, Y_train)
        Y_knn_prediction = nbrs.predict(X_validate)
        weights['mse'] = np.sqrt(mean_squared_error(Y_knn_prediction, np.array(Y_validate)))
        stats.append(weights.copy())
    weights[crit] = default_weights[crit]

import json

print(stats)
print("Stats obtained")


text_file = open("Output.json", "w")

text_file.write(json.dumps(stats))

text_file.close()
