import pandas as pd
from datetime import datetime
from sklearn.metrics import mean_squared_error

def preprocess(df):
    df = df.drop(columns=['date_modified','available_spot_number','minute'])    
    df.fillna(0)
    return df

data = pd.read_csv('../data/parkings_measurements_202205040833.csv', delimiter = ';', low_memory=False)

data['datetime'] = data.apply(lambda row: datetime.fromtimestamp(row.date_modified/1000), axis=1)
data['day_of_week'] = data['datetime'].dt.day_of_week
data['year'] = pd.DatetimeIndex(data['datetime']).year
data['month'] = pd.DatetimeIndex(data['datetime']).month
data['day'] = pd.DatetimeIndex(data['datetime']).day
data['hour'] = pd.DatetimeIndex(data['datetime']).hour
data['minute'] = pd.DatetimeIndex(data['datetime']).minute

data.to_csv('../data/data.csv', index=False)
data = data.groupby(['source_id','year','month','day','hour', 'day_of_week']).mean().reset_index()
data.to_csv('../data/data_hour.csv', index=False)