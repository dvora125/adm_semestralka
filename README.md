# adm_semestralka

Repozitář pro semestrální úlohu z NI-ADM v AR 2021/2022
Aby model(y) fungoval(y), je potřeba mít (trénovací/testovací) data ve složce data, která nejsou veřejná a navíc jsou velká, proto nejsou v repozitáři, máme je každý lokálně.

```
├── data
│   ├── parkings_202205040856.csv
│   └── parkings_measurements_202205040833.csv
```